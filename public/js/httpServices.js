$(function() {
	$('#signupForm').submit(function(e){
		e.preventDefault();
		$('.alert').remove();
		var formdata = $(this).serialize();
		console.log(formdata, 'formdata')
		$.ajax({
			type:'post',
			url:serverPath+'/signup',
			data: formdata,
			success: (res) => {
				console.log(res)
				if (res !== 1) {
					for(let v in res) {
						//console.log(res[v][0], 'v')
						let str = '<div class="alert alert-danger">';
						str+=res[v][0];
						str+= '</div>';
						console.log('#signupForm input[name="'+v+'"]')
						$('#signupForm input[name="'+v+'"]').after(str);
					}
				} else {
					window.location.href="/";
				}
			}, error: (err) => {
				console.log(err)
			}
		})
	})

	$('#loginForm').submit(function(e){
		e.preventDefault();
		$('.alert').remove();
		var formdata = $(this).serialize();
		console.log(formdata, 'formdata')
		$.ajax({
			type:'post',
			url:serverPath+'/login',
			data: formdata,
			success: (res) => {
				console.log(res)
				if (res != 1) {
					for(let v in res) {
						//console.log(res[v][0], 'v')
						let str = '<div class="alert alert-danger">';
						str+=res[v][0];
						str+= '</div>';
						console.log('#loginForm input[name="'+v+'"]')
						$('#loginForm input[name="'+v+'"]').after(str);
					}
				} else {
					console.log(res, 'test')
					window.location.href="/";
				}
			}, error: (err) => {
				console.log(err)
			}
		})
	})
	
	$('#categoryForm').submit(function(e){
		e.preventDefault();
		var formObj = $(this);
		$('.alert').remove();
		var formdata = $(this).serialize();
		console.log(formdata, 'formdata')
		$.ajax({
			type:'post',
			url:serverPath+'/admin/category',
			data: formdata,
			success: (res) => {
				console.log(res)
				if (res != 1) {
					for(let v in res) {
						//console.log(res[v][0], 'v')
						let str = '<div class="alert alert-danger">';
						str+=res[v][0];
						str+= '</div>';
						console.log('#categoryForm input[name="'+v+'"]')
						$('#categoryForm input[name="'+v+'"]').after(str);
					}
				} else {
					console.log(123)
					$('.message').html('Category has been saved');
					
				}
			}, error: (err) => {
				console.log(err)
			}
		})
	})
	$('#brandForm').submit(function(e){
		e.preventDefault();
		var formObj = $(this);
		$('.alert').remove();
		var formdata = $(this).serialize();
		console.log(formdata, 'formdata')
		$.ajax({
			type:'post',
			url:serverPath+'/admin/brand',
			data: formdata,
			success: (res) => {
				console.log(res)
				if (res != 1) {
					for(let v in res) {
						//console.log(res[v][0], 'v')
						let str = '<div class="alert alert-danger">';
						str+=res[v][0];
						str+= '</div>';
						console.log('#brandForm input[name="'+v+'"]')
						$('#brandForm input[name="'+v+'"]').after(str);
					}
				} else {
					console.log(123)
					$('.message').html('Brand has been saved');
					
				}
			}, error: (err) => {
				console.log(err)
			}
		})
	})
	
	$('#productForm').submit(function(e){
		e.preventDefault();
		$('.alert').remove();
		var formObj = $(this);
		var form = $('#productForm')[0]; 
		var formdata = new FormData(form);
		//console.log(formdata,'formdata');
		$.ajax({
			type:'post',
			url:serverPath+'/admin/product',
			data: formdata,
			contentType: false,
			processData: false,
			success: (res) => {
				console.log(res)
				if (res != 1) {
					for(let v in res) {
						//console.log(res[v][0], 'v')
						let str = '<div class="alert alert-danger">';
						str+=res[v][0];
						str+= '</div>';
						//console.log('#productForm input[name="'+v+'"]')
						$('#productForm input[name="'+v+'"],#productForm textarea[name="'+v+'"], #productForm select[name="'+v+'"]').after(str);
					}
				} else {
					$('.message').html('Product has been saved');
					formObj.trigger('reset');
				}
			}, error: (err) => {
				console.log(err)
			}
		})
	})
	var categories = [];
	var brands = [];
	var min = 0;
	var max = 0;
	$('.category-filter, .brand-filter,.price-filter').change(function(e){
		var type = $(this).attr('name');
		var value = $(this).val();
		if (type === 'category') {
			if ($(this).is(':checked')) {
				categories.push(value);
			} else {
				var key = categories.indexOf(value);
				//delete categories[key];
				categories.splice(key, 1);
			}
			console.log(categories, 'categories')
		} else if (type === 'brand') {	
			if ($(this).is(':checked')) {
				brands.push(value);
			} else {
				var key = brands.indexOf(value);
				//delete categories[key];
				brands.splice(key, 1);
			}
			console.log(brands)
		}  else if (type === 'price') {
			values = value.split('-');
			min = values[0];
			max = values[1];
		}
			$.ajax({
				type:'post',
				data: {categories: categories, brands: brands, min : min, max: max},
				url: serverPath + '/filter-products',
				success: function(res) {
				//	console.log(res)
				if (res.code === 200) {
					var str = "";
					for (var i = 0; i < res.products.length; i++) {
						str += '<div class="col-sm-4"> ' +
						'<div class="product-image-wrapper"> ' +
								'<div class="single-products"> ' +
									'<div class="productinfo text-center">' +
										'<img src="' +serverPath+'/storage/'+res.products[i].path + '" alt="" />'+
										'<h2>'+res.products[i].price+'</h2>'+
										'<p> '+res.products[i].name.substring(0,20)+'</p>'+
										'<a href="#" class="btn btn-default add-to-cart" data="'+res.products[i].id+'"><i class="fa fa-shopping-cart"></i>Add to cart</a>' +
									'</div>' +
									'<div class="product-overlay">'+
										'<div class="overlay-content">'+
											'<h2>'+res.products[i].price+'</h2>'+
											'<p>'+res.products[i].name.substring(0, 20)+'</p>'+
											'<a href="#" class="btn btn-default add-to-cart" data="'+res.products[i].id+'"><i class="fa fa-shopping-cart"></i>Add to cart</a>'+
										'</div>'+
									'</div>'+
							'</div>'+
							'<div class="choose">'+
								'<ul class="nav nav-pills nav-justified">' +
									'<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>' +
									'<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>' +
								'</ul> '+
							'</div>' +
						'</div>'+
					'</div>';
					}
					$('.features_items').html(str);
				}
				}, error: function(err) {
					console.error(err, 'error')
				}
			})
		
	})

	//add to cart
	$(document).on('click', '.add-to-cart', function(e ) {
		e.preventDefault();
		var id = $(this).attr('data');
		var productIds = localStorage.getItem('products');
		if (!productIds) {
			localStorage.setItem('products', id);
			alert('product  has been added');
		} else {
		  var ids = 	productIds.split(',');
		  if (ids.indexOf(id) === -1) {
			  ids.push(id);
			  ids.join(',');
			 localStorage.setItem('products', ids);
			 alert('product  has been added');
		  }	else {
			alert('product already added!');
		  }
		}
	})
	
	if (+cart === 1) {
	   var	products = localStorage.getItem('products');
		$.ajax({
			type:'post',
			data: {products: products},
			url: serverPath + '/filter-products',
			success: function(res) {
			console.log(res)
			if (res.code === 200) {
				var str = "";
				for (var i = 0; i < res.products.length; i++) {
					str += '<div class="col-sm-4 cart-product"> ' +
					'<div class="product-image-wrapper"> ' +
							'<div class="single-products"> ' +
								'<div class="productinfo text-center">' +
									'<img src="' +serverPath+'/storage/'+res.products[i].path + '" alt="" />'+
									'<h2>'+res.products[i].price+'</h2>'+
									'<p> '+res.products[i].name.substring(0,20)+'</p>'+
									'<a href="#" class="btn btn-default remove-to-cart" data="'+res.products[i].id+'"><i class="fa fa-shopping-cart"></i>Remove to cart</a>' +
								'</div>' +
								'<div class="product-overlay">'+
									'<div class="overlay-content">'+
										'<h2>'+res.products[i].price+'</h2>'+
										'<p>'+res.products[i].name.substring(0, 20)+'</p>'+
										'<a href="#" class="btn btn-default remove-to-cart" data="'+res.products[i].id+'"><i class="fa fa-shopping-cart"></i>Remove to cart</a>'+
									'</div>'+
								'</div>'+
						'</div>'+
						'<div class="choose">'+
							'<ul class="nav nav-pills nav-justified">' +
								'<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>' +
								'<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>' +
							'</ul> '+
						'</div>' +
					'</div>'+
				'</div>';
				}
				$('.cart_items').html(str);
			}
			}, error: function(err) {
				console.error(err, 'error')
			}
		})
	}
	//remove cart
	$(document).on('click', '.remove-to-cart', function(e ) {
		e.preventDefault();
		var id = $(this).attr('data');
		var productIds = localStorage.getItem('products');
		  var ids = productIds.split(',');
		  if (ids.indexOf(id) !== -1) {
			var key = ids.indexOf(id);
			  ids.splice(key, 1);
			  ids.join(',');
			 localStorage.setItem('products', ids);
			 $(this).parentsUntil('.cart-product').parent().remove();
			 alert('product  has been removed');
		  }	else {
			alert('product not found!');
		  }
	})
	// documents end
})


