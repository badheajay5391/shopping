@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<div class="signup-form"><!--sign up form-->
				<h2>New User Signup!</h2>
				<form action="signup" id="signupForm" method="POST" >
					@csrf
					<input type="text" name='name' placeholder="Name"/>
					
					<input type="email" name='email' placeholder="Email Address"/>
					<input type="password" name='password' placeholder="Password"/>
					<input type="password" name='password_confirmation' placeholder="Confirm Password"/>
					<button type="submit" class="btn btn-default">Signup</button>
				</form>
			</div><!--/sign up form-->
		</div>
	</div>
@endsection