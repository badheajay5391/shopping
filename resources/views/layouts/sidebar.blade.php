<div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            @if (!empty($categories))
                                @foreach($categories as $category)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#"> <input type="checkbox" name="category" class="category-filter" value="{{$category->id}}" />{{$category->category}}</a></h4>
                                </div>
                            </div>
                                @endforeach
                            @endif
                        </div><!--/category-products-->
                    
                        <div class="brands_products"><!--brands_products-->
                            <h2>Brands</h2>
                            <div class="brands-name">
                                <ul class="nav nav-pills nav-stacked">
                                @if (!empty($brands))
                                    @foreach($brands as $brand)
                                        <li><a href="#"><input type="checkbox" name="brand" class="brand-filter" value="{{$brand->id}}" /> {{$brand->brand}}</a></li>
                                    @endforeach
                                @endif
                                </ul>
                            </div>
                        </div><!--/brands_products-->
                        
                        <div class="price-range"><!--price-range-->
                            <h2>Price Range</h2>
                            <div class="well text-center">
                                 <input type="text" name="price" class="price-filter" value=""  ><br />
                                 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                            </div>
                        </div><!--/price-range-->
                        
                        <div class="shipping text-center"><!--shipping-->
                            <img src="{{url('/')}}/{{url('/')}}/images/home/shipping.jpg" alt="" />
                        </div><!--/shipping-->
                    
                    </div>
                </div>
               