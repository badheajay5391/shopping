<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.css')
    <script type="text/javascript">
        var serverPath = `{{url('/')}}`;
        var cart = `{{ Request::is('cart') }}`;
    </script>

</head>
<body>
    <div id="app">
        @include('layouts.header')
        <main>
            @yield('content')
        </main>
        @include('layouts.footer')
    </div>
    @include('layouts.scripts')
</body>
</html>
