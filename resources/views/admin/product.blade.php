@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
					<div class="login-form">
						<div class='alert-success message' ></div>
						<h2>Product</h2>
						<form id="productForm" action="product">
							@csrf
							<input type="text" name="name" placeholder="product name" />
							<select name="category" class="form-control">
								<option value=''>select category</option>
								
								@if (!empty($categories))
									@foreach($categories as $key => $val)
										<option value="{{ $val->id }}">{{ $val->category }}</option>
									@endforeach
								@endif
							</select>
							<br/>
							<select name="brand" class="form-control">
								<option value=''>select brand</option>
								@if (!empty($brands))
									@foreach($brands as $key => $val)
										<option value="{{ $val->id }}">{{ $val->brand }}</option>
									@endforeach
								@endif
							</select>
							<br />
							<input type="text" name="price" placeholder="product Price" />
							<input type="file" name="file" placeholder="product file" />
							<textarea class="form-control" name="desc" placeholder="product description"></textarea>
							<button type="submit" class="btn btn-default">Save</button>
						</form>
					</div><!--/login form-->
				</div>
			</div>
@endsection