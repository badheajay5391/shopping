@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
					<div class="login-form">
						<div class='alert-success message' ></div>
						<h2>Category</h2>
						<form id="categoryForm" action="category">
							@csrf
							<input type="text" name="category" placeholder="category" />
							<button type="submit" class="btn btn-default">Save</button>
						</form>
					</div><!--/login form-->
				</div>
			</div>
@endsection