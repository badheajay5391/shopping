@extends('layouts.app')

@section('content')
	<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
					<div class="login-form">
						<div class='alert-success message' ></div>
						<h2>Brand</h2>
						<form id="brandForm" action="brand">
							@csrf
							<input type="text" name="brand" placeholder="brand" />
							<button type="submit" class="btn btn-default">Save</button>
						</form>
					</div><!--/login form-->
				</div>
			</div>
@endsection