<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index');
Route::post('filter-products', 'ProductController@filterProducts');
Route::get('cart', 'ProductController@cart');
Route::get('pay', 'ProductController@pay');

Route::post('subscribe-cancel', [
    'as' => 'subscribe-cancel',
    'uses' => 'ProductController@SubscribeCancel'
]);

Route::post('subscribe-response', [
    'as' => 'subscribe-response',
    'uses' => 'ProductController@SubscribeResponse'
]);

Route::get('login', [ 'as' => 'login', 'uses' => 'UserController@index']);
Route::post('login', 'UserController@signIn');
Route::get('signup', 'UserController@signup');
Route::post('signup', 'UserController@signupSave');
Route::get('logout', 'UserController@logout');

// email send
Route::get('send', 'ProductController@send');


/*
=====================
Admin route
======================
*/
/*Route::middleware('admin')->group(function() {
	Route::get('/admin/category', 'CategoryController@index');
});
*/

Route::prefix('admin')->middleware('admin')->group(function() {
	Route::get('/category', 'CategoryController@index');
	Route::post('/category', 'CategoryController@save');
	Route::get('/brand', 'BrandController@index');
	Route::post('/brand', 'BrandController@save');
	Route::get('/product', 'ProductController@product');
	Route::post('/product', 'ProductController@save');
});