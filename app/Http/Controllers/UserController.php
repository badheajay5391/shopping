<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Events\UserLoggedIn;

class UserController extends Controller
{ 
    public function __construct() 
    {
        $this->middleware('guest')->except('logout');
    }
    public function index()
    {
    	return view('pages.login');
    }
    public function signIn(Request $request) 
    {
        $credentials = $request->only('email','password');
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'min:8'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        if (!Auth::attempt($credentials, 1)) {
            return response()->json(['error'=> 'Invalid user or password'], 200);
        }
        echo 1;
    }   
    public function signup()
    {
    	return view('pages.signup');
    }
    public function signupSave(Request $request) 
    {	
    	$data = $request->all();

    	$validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);

    	if ($validator->fails()) {
    		return response()->json($validator->messages(), 200);
    	}

    	User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

    	echo 1;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
