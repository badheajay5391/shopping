<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    public function index()
    {
    	return view('admin.category');
    }
    public function save(Request $request) 
    {
        $data = $request->only('category');

        $validator = Validator::make($request->all(), [
            'category' => ['required', 'string', 'max:100', 'unique:categories'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }
        
        $obj = new Category();
        $obj->category = $data['category'];
        $obj->save();
        echo 1;
    } 
}
