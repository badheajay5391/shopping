<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use Illuminate\Support\Facades\DB;
use Log;
use Mail;
use App\Jobs\SendWelcomeEmail;
use Illuminate\Support\Facades\Redis;
use App\Events\OrderShipped;

class ProductController extends Controller
{
    public function index()
    {
        
       /*  Redis::hmset("tutorialspoint",["name"=>"aj", "mob"=> '77095478997']); 
        $arList = Redis::hget('tutorialspoint', "name");
        echo "Stored keys in redis:: " ;
        print_r($arList); die; */
       Redis::publish('test-channel', json_encode(['foo' => 'bar']));
       $order = ['name'=>'T Shirt', 'price'=> 2000];
       event(new OrderShipped($order)); 

        $model = new Product();
        $products = $model->products();
        /*echo "<pre>";
        print_r($products);die;*/
        // print last query
        //DB::enableQueryLog();
        /*$brands = Product::find(2);
        
        echo "<pre>12";
        print_r($brands);
        print_r($brands->brand);
        print_r($brands->category);
        die;*/  
        
      // $brandProducts = Brand::get();
        /*$query = DB::getQueryLog();
        print_r($query);*/
        /*echo "<pre>12";
        print_r($brandProducts[0]->products);
        die;
        */
    	return view('index', ['products' => $products]);
    }

    public function product()
    {
        $formdata['categories'] = Category::get();
        $formdata['brands'] = Brand::get();
    	return view('admin.product', $formdata);
    }

    public function save(Request $request) 
    {
        $data = $request->all();
        //extract($request->all()); 
        $messages = [
            'required' => 'The :attribute field is required.',
        ];
        
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:100'],
            'category' => ['required', 'numeric'],
            'brand' => ['required', 'numeric'],
            'desc' => ['required', 'string'],
            'price' => ['required', 'numeric'],
            'file' => ['required','max:10000', 'image']
        ], $messages);

        // 'dimensions:min_width=100,min_height=200'
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $path = $request->file->store('images');
       // echo $path;die;

       $product = new Product();
       $product->name = $data['name'];
       $product->path = $path;
       $product->desc = $data['desc'];
       $product->price = $data['price'];
       $product->category_id = $data['category'];
       $product->brand_id = $data['brand'];
       $product->save();
       echo 1;
       
    } 

    public function filterProducts(Request $request)
    {
        $data = $request->all();
        $model  = new Product();
        $products = $model->products($data);
        return response()->json(['code'=> 200, 'products' => $products]);
    }

    public function cart(Request $request) 
    {
        return view('pages.cart' );
    }

    public function pay(Request $request)
    {   
        return view('pages.payment');
    }
    public function SubscribeResponse(Request $request)
    {
        echo "<pre>";
        print_r($request->all());
        dd('Payment Successfully done!');
    }
    public function SubscribeCancel(Request $reques)
    {
        echo "<pre>";
        print_r($request->all());
        dd('Payment Cancel!');
    }

    public function send()
    {
        Log::info("Request Cycle with Queues Begins");
        $this->dispatch((new SendWelcomeEmail())->delay(60 * 1));
        Log::info("Request Cycle with Queues Ends");
      
    }

}
