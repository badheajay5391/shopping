<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Brand; 

class BrandController extends Controller
{
    public function index()
    {
    	return view('admin.brand');
    }
    public function save(Request $request) 
    {
        $data = $request->only('brand');

        $validator = Validator::make($request->all(), [
            'brand' => ['required', 'string', 'max:100', 'unique:brands'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }
        
        $obj = new brand();
        $obj->brand = $data['brand'];
        $obj->save();
        echo 1;
    } 
}
