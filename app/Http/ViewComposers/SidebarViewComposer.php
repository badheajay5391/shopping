<?php
namespace App\Http\ViewComposers;

use App\Models\Brand;
use App\Models\Category;

class SidebarViewComposer
{
    public function compose($view)
    {
        $brands = Brand::get();
        $categories = Category::get();
        $view->with('brands', $brands);
        $view->with('categories', $categories);
    }
}
?>