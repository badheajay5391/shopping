<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$user = Auth::user();
        echo "<pre>";
        print_r($user);die;*/
        
        if (!Auth::isAdmin()) {
            throw new Exception("User can not authorise for this route", 1);
        }
        return $next($request);
    }
}
