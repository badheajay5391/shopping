<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $table = "products as p";
    /** join query */
    public function products($data = [])
    {
        $product = $this->newQuery();
        
        $product
            ->select('p.id','name', 'price', 'desc', 'path', 'c.category', 'b.brand')           
            ->join('categories as c', 'p.category_id', '=', 'c.id')
            ->join('brands as b', 'b.id', '=', 'p.brand_id');

        if (!empty($data)) {
            // category where 
            if (isset($data['categories'])) {
                $product->whereIn('category_id' ,$data['categories']);
            }
            // brands where
            if (isset($data['brands'])) {
                $product->whereIn('brand_id', $data['brands']);
            }
            //price
            if (isset($data['min']) && isset($data['max']) ) {
                $min = $data['min'];
                $max = $data['max'];
                $product->whereBetween('price', [$min, $max]);
            }
            //cart
            if (isset($data['products'])) {
                $products =  explode(',',$data['products']);
                $product->whereIn('p.id',$products);
            }
        }  
       
        $results = $product->get();
       
        return $results;
    }
    /** product to brand relation ship, one to one, inverse relation */
    public function brand() {
        return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    } 
    public function category() {
        return $this->belongsTo('App\Models\Category');
    } 
}
